var doacoes = require('../controllers').doacoes;

module.exports = (route) => {
  route.get('/doacao', doacoes.list);
  route.post('/doacao', doacoes.create);
  route.get('/doacao/:id', doacoes.select)
  route.put('/doacao/:id', doacoes.update);
  route.delete('/doacao/:id', doacoes.remove);
}
