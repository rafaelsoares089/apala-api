var cargos = require('../controllers').cargos;

module.exports = (route) => {
  route.get('/cargo', cargos.list);
  route.post('/cargo', cargos.create);
  route.get('/cargo/:id', cargos.select)
  route.put('/cargo/:id', cargos.update);
  route.delete('/cargo/:id', cargos.remove);
}
