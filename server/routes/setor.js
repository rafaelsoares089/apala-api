var setores = require('../controllers').setores;

module.exports = (route) => {
  route.get('/setor', setores.list);
  route.post('/setor', setores.create);
  route.get('/setor/:id', setores.select);
  route.put('/setor/:id', setores.update);
  route.delete('/setor/:id', setores.remove);
}
