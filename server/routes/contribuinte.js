var contribuintes = require('../controllers').contribuintes;

module.exports = (route) => {
  route.get('/contribuinte', contribuintes.list);
  route.post('/contribuinte', contribuintes.create);
  route.get('/contribuinte/:id', contribuintes.select)
  route.put('/contribuinte/:id', contribuintes.update);
  route.delete('/contribuinte/:id', contribuintes.remove);
}
