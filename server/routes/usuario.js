var usuarios = require('../controllers').usuarios;
module.exports = (route) => {
  route.get('/usuario',usuarios.list);
  route.get('/usuario/:id', usuarios.select);
  route.put('/usuario/:id', usuarios.update);
  route.delete('/usuario/:id', usuarios.remove);
}
