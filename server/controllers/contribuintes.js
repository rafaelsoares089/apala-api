const Contribuinte = require('../models').Contribuinte;
const Doacao = require('../models').Doacao;
module.exports = {
  list (req, res) {
    return Contribuinte
      .findAll({include:[{model: Doacao, as: 'doacoes'}]})
      .then(contribuintes => res.json(contribuintes))
      .catch(err => res.status(400).send({message:err.toString()}))
  },
  create (req, res) {
    return Contribuinte
      .create(req.body)
      .then(contribuinte => res.json(contribuinte))
      .catch(err => res.status(400).send({message:err.toString()}))
  },
  select (req, res) {
    return Contribuinte
      .find({ where: { id:req.params.id } , include:[{model:Doacao, as: 'doacoes'}]})
      .then(contribuinte => res.json(contribuinte))
      .catch(err => res.status(400).send({message:err.toString()}))
  },
  update (req, res) {
    return Contribuinte
      .find({ where: { id:req.params.id } })
      .then(contribuinte => Object.assign(contribuinte, req.body).save().then(()=> res.json(contribuinte)))
      .catch(err => res.status(400).send({message:err.toString()}))
  },
  remove (req, res) {
    return Contribuinte
      .find({ where: { id: req.params.id }})
      .then(contribuinte => contribuinte.destroy().then(()=> res.json({message:"removido"})))
      .catch(err => res.status(400).send({message:err.toString()}))
  }
}
