const usuarios = require('./usuarios');
const authentications = require('./authentications');
const setores = require('./setores');
const cargos = require('./cargos');
const contribuintes = require('./contribuintes');
const doacoes = require('./doacoes');

module.exports = {
  usuarios,
  setores,
  authentications,
  cargos,
  contribuintes,
  doacoes,
};
